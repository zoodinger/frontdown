package com.zoodinger.frontdown;

import javax.swing.*;
import java.awt.*;
import java.time.Year;

public class AboutWindow extends JDialog {
    private JButton button;

    private String makeBold(String string) {
        return "<html><b>%s</b></html>".formatted(string);
    }

    private Component makeInformation() {
        @SuppressWarnings("UnnecessaryUnicodeEscape") // Using the character itself causes a display glitch
        var copyright = "<html>\u00a9 2021-%d</html>".formatted(Year.now().getValue());

        return new FormBuilder()
                .setGaps(Padding.VALUE, 0)
                .setLabelAlignment(FormBuilder.LabelAlignmentX.LEFT)
                .add(makeBold("Version "), new JLabel(Resources.getVersion()))
                .add(makeBold("Author "), new JLabel(Resources.getAuthor()))
                .add(makeBold("Copyright "), new JLabel(copyright))
                .build();
    }

    private Component makeTop() {
        var panel = new JPanel();
        var layout = new BoxLayout(panel, BoxLayout.X_AXIS);

        panel.add(new JLabel(Resources.ABOUT_ICON));
        panel.add(Box.createHorizontalStrut(Padding.VALUE * 2));
        panel.add(makeInformation());
        panel.add(Box.createHorizontalGlue());

        panel.setLayout(layout);
        return panel;
    }

    private Component makeCenter() {
        var message = """
                <html>
                Frontdown helps with backing up and restoring projects<br>
                or any other important documents based on a root folder.<br>
                It can be used in conjunction with cloud storage solutions<br>
                and versioning systems.
                </html>""";

        return new JLabel(message);
    }

    private Component makeBottom() {
        var panel = new JPanel();
        var layout = new BoxLayout(panel, BoxLayout.X_AXIS);

        panel.add(Box.createHorizontalGlue());
        panel.add(button = new JButton("OK"));
        panel.add(Box.createHorizontalGlue());

        panel.setLayout(layout);

        return panel;
    }

    private void makeLayout() {
        var panel = new JPanel();
        var layout = new BorderLayout(0, Padding.VALUE);
        panel.setLayout(layout);

        panel.add(makeTop(), BorderLayout.PAGE_START);
        panel.add(makeCenter(), BorderLayout.CENTER);
        panel.add(makeBottom(), BorderLayout.PAGE_END);

        var border = BorderFactory.createEmptyBorder(
                Padding.VALUE, Padding.VALUE * 2, Padding.VALUE, Padding.VALUE * 2
        );
        panel.setBorder(border);

        setContentPane(panel);
    }

    public AboutWindow(JFrame parent) {
        super(parent, "About Frontdown", true);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        makeLayout();

        getRootPane().setDefaultButton(button);
        button.addActionListener(e -> dispose());

        pack();

        setResizable(false);
        setLocationRelativeTo(parent);

        setVisible(true);
    }
}
