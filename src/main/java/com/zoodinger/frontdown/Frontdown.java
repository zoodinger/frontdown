package com.zoodinger.frontdown;

import io.github.sanyarnd.applocker.AppLocker;

import javax.swing.*;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;

public final class Frontdown {

    private static final String UNIQUE_ID = Frontdown.class.getName();
    private static final String BRING_TO_FRONT = "bringToFront";

    private static Configuration configuration;
    private static AppLocker lock;

    private Frontdown() {
    }

    private static Serializable onReceiveMessage(Serializable message) {
        if (BRING_TO_FRONT.equals(message)) {
            SwingUtilities.invokeLater(() -> MainWindow.getMainInstance().toFront());
        }
        return null;
    }

    private static void onLockFail() {
        System.err.println("Failed to receive application lock.");
        System.exit(1);
    }

    private static void onLockBusy() {
        System.err.println("Frontdown is already running.");
        System.exit(1);
    }

    private static void start() {
        SwingUtilities.invokeLater(() -> MainWindow.createNewInstance(configuration));
    }

    public static void main(String[] args) {
        var configPath = PathUtils.getConfigPath();

        try {
            configuration = Configuration.readFromDisk();
        } catch (Exception e) {
            ThemeManager.setDefaultTheme();

            var sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            var error = sw.toString().split("\n")[0];

            JOptionPane.showMessageDialog(null,
                    "Failed to read configuration\n\n%s".formatted(error),
                    "Configuration Error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }

        lock = AppLocker.create(UNIQUE_ID)
                .setPath(configPath)
                .onSuccess(Frontdown::start)
                .onBusy(BRING_TO_FRONT, Frontdown::onLockBusy)
                .setMessageHandler(Frontdown::onReceiveMessage)
                .onFail(Frontdown::onLockFail)
                .build();

        lock.lock();
    }

    public static void Shutdown() {
        if (lock != null) {
            lock.unlock();
        }
    }
}