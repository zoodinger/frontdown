package com.zoodinger.frontdown;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.concurrent.CancellationException;

public class ProgressDialog extends JDialog implements PropertyChangeListener {

    private static final String PROGRESS_EVENT = "progress";
    private static final String DETERMINATE_EVENT = "initialize";
    private static final String MESSAGE_EVENT = "message";
    private static final String CAPTION_EVENT = "caption";
    private static final String STATE_EVENT = "state";
    private static final String TITLE_EVENT = "title";

    private final SwingWorker<Void, Void> worker;
    private JProgressBar progressBar;
    private JLabel captionLabel;
    private JLabel messageLabel;
    private JPanel cardLayoutParent;
    private CardLayout cardLayout;
    private JButton cancelButton;
    private JButton okButton;

    private static final String OK_CARD = "ok";
    private static final String CANCEL_CARD = "cancel";

    private boolean isFinished = false;

    public ProgressDialog(JFrame parent, Task task, String title) {
        this(parent, task, title, "", "");
    }

    private void setAllSizes(Component component, Dimension size) {
        if (size == null) {
            size = component.getSize();
        }
        component.setMinimumSize(size);
        component.setPreferredSize(size);
        component.setMaximumSize(size);
    }

    public ProgressDialog(JFrame parent, Task task, String title, String caption, String message) {
        super(parent, title, true);

        worker = new InnerWorker(task);

        makeLayout();
        bindBehaviours();

        pack();

        setAllSizes(messageLabel, null);
        setAllSizes(captionLabel, null);

        var size = messageLabel.getSize();
        setAllSizes(progressBar, size);

        progressBar.setStringPainted(true);
        progressBar.setIndeterminate(true);

        // Remove focus from the "Cancel" button
        getRootPane().grabFocus();

        getRootPane().setDefaultButton(okButton);

        setResizable(false);

        messageLabel.setText(message);
        captionLabel.setText(caption);

        setLocationRelativeTo(parent);

        worker.addPropertyChangeListener(this);
        worker.execute();

        setVisible(true);
    }

    public void setCaption(String caption) {
        captionLabel.setText(caption);
    }

    public void setMessage(String message) {
        messageLabel.setText(message);
    }

    /**
     * Proxy class to interface between a ProgressDialog and a ProgressDialog.Task
     */
    public static class Worker {
        private final InnerWorker innerWorker;

        public boolean isCancelled() {
            return innerWorker.isCancelled();
        }

        private Worker(InnerWorker innerWorker) {
            this.innerWorker = innerWorker;
        }

        public void setMessage(String message) {
            innerWorker.firePropertyChange(MESSAGE_EVENT, null, message);
        }

        public void setCaption(String caption) {
            innerWorker.firePropertyChange(CAPTION_EVENT, null, caption);
        }

        public void setTitle(String title) {
            innerWorker.firePropertyChange(TITLE_EVENT, null, title);
        }

        public void setProgressRatio(double progress) {
            setProgress((int) (progress * 100));
        }

        public void setProgress(int progress) {
            innerWorker.firePropertyChange(PROGRESS_EVENT, null, Math.max(Math.min(100, progress), 0));
        }

        public void showProgress(boolean show) {
            innerWorker.firePropertyChange(DETERMINATE_EVENT, null, show);
        }
    }

    private Component makeTop() {
        var panel = new JPanel(new GridBagLayout());

        var row = new GridBagConstraints();
        row.fill = GridBagConstraints.NONE;
        row.anchor = GridBagConstraints.LINE_START;

        row.insets = new Insets(0, 0, Padding.HALF_VALUE, 0);

        captionLabel = new JLabel(Padding.WIDE_TEXT);
        progressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
        messageLabel = new JLabel(Padding.WIDE_TEXT);

        row.gridy = 0;
        panel.add(captionLabel, row);
        row.gridy = 1;
        panel.add(progressBar, row);
        row.gridy = 2;
        row.insets = new Insets(0, 0, 0, 0);
        panel.add(messageLabel, row);

        return panel;
    }

    private JPanel makeBottomCardFor(Component component) {
        var panel = new JPanel(new BorderLayout(0, Padding.HALF_VALUE));
        panel.add(component, BorderLayout.LINE_END);
        return panel;
    }

    private Component makeBottom() {
        var panel = new JPanel();
        var layout = new CardLayout();
        panel.setLayout(layout);

        cancelButton = new JButton("Cancel");
        okButton = new JButton("OK");
        panel.add(makeBottomCardFor(cancelButton), CANCEL_CARD);
        panel.add(makeBottomCardFor(okButton), OK_CARD);

        layout.show(panel, CANCEL_CARD);

        cardLayout = layout;
        cardLayoutParent = panel;

        return panel;
    }

    private void makeLayout() {
        var panel = new JPanel();
        var layout = new BorderLayout(0, Padding.VALUE);
        panel.setLayout(layout);
        panel.setBorder(Padding.makeBorder(Padding.VALUE));
        panel.add(makeTop(), BorderLayout.PAGE_START);
        panel.add(makeBottom(), BorderLayout.PAGE_END);

        setContentPane(panel);
    }

    private void bindBehaviours() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        okButton.addActionListener(e -> dispose());
        cancelButton.addActionListener(e -> onCancel());

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (isFinished) {
                    dispose();
                } else {
                    onCancel();
                }
            }
        });
    }

    private void onCancel() {
        worker.cancel(true);
        cancelButton.setEnabled(false);
    }

    private void showMessage(Message message) {
        if (message == null) {
            return;
        }

        var messageType = switch (message.type()) {
            case PLAIN -> JOptionPane.PLAIN_MESSAGE;
            case INFO -> JOptionPane.INFORMATION_MESSAGE;
            case WARNING -> JOptionPane.WARNING_MESSAGE;
            case ERROR -> JOptionPane.ERROR_MESSAGE;
        };

        JOptionPane.showMessageDialog(this, message.message(), message.title(), messageType);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case PROGRESS_EVENT -> progressBar.setValue((Integer) evt.getNewValue());
            case MESSAGE_EVENT -> setMessage((String) evt.getNewValue());
            case CAPTION_EVENT -> setCaption((String) evt.getNewValue());
            case TITLE_EVENT -> setTitle((String) evt.getNewValue());
            case DETERMINATE_EVENT -> {
                var value = (Boolean) evt.getNewValue();
                progressBar.setIndeterminate(!value);
                progressBar.setStringPainted(value);
            }
        }
    }

    private class InnerWorker extends SwingWorker<Void, Void> {
        private final Task task;
        private final Worker worker;

        private InnerWorker(Task task) {
            this.task = task;
            worker = new Worker(this);
        }

        @Override
        protected Void doInBackground() {
            var isCancelledFromInside = false;

            try {
                task.setWorker(worker);
                task.run();
            } catch (CancellationException e) {
                // Cancelling the task from the UI doesn't actually interrupt it.
                // We need to rethrow a CancellationException from inside
                isCancelledFromInside = true;
            } catch (Exception e) {
                // All other exceptions treated like errors
                SwingUtilities.invokeLater(() -> {
                    showMessage(task.onError(e));
                    dispose();
                });
                return null;
            }

            if (isCancelledFromInside || isCancelled()) {
                // Cancelled either from the Progress UI (isCancelled()) or from within
                // the task's run() method (by throwing a CancellationException).
                SwingUtilities.invokeLater(() -> {
                    showMessage(task.onCancel());
                    dispose();
                });
            } else {
                // We're here only if the task finished successfully without any errors or getting cancelled.
                SwingUtilities.invokeLater(() -> {
                    task.onDone();
                    cardLayout.show(cardLayoutParent, OK_CARD);
                    isFinished = true;
                });
            }

            return null;
        }
    }

    public enum MessageType {
        PLAIN, INFO, WARNING, ERROR
    }

    public record Message(String title, String message, MessageType type) {
        public static Message makeError(String message) {
            return new Message("Error", message, MessageType.ERROR);
        }

        public static Message makeWarning(String message) {
            return new Message("Warning", message, MessageType.WARNING);
        }

        public static Message makeInfo(String title, String message) {
            return new Message(title, message, MessageType.INFO);
        }
    }

    public abstract static class Task {
        private Worker worker;

        private void setWorker(Worker worker) {
            if (this.worker != null) {
                throw new IllegalStateException("Task cannot be shared");
            }

            this.worker = worker;
        }

        protected Worker getWorker() {
            return worker;
        }

        abstract void run() throws Exception;

        protected Message onError(Exception exception) {
            return Message.makeError(exception.getMessage());
        }

        protected Message onCancel() {
            return null;
        }

        protected void onDone() {

        }
    }
}
