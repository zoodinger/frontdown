package com.zoodinger.frontdown;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.Consumer;

public class MainWindow extends JFrame {
    private final Configuration configuration;

    private static MainWindow instance;
    private static final int VISIBLE_ROW_COUNT = 10;

    private JButton openProjectButton;
    private JButton openBackupsButton;
    private JButton restoreProjectButton;
    private JButton backupProjectButton;

    private JList<Project> projectList;

    private JPopupMenu popupMenu;
    private JMenu projectMenu;

    private WindowListener shutdownHook;
    private Map<Command, CmdAction> cmdActions;
    private CmdAction defaultCmdAction;

    private Component[] projectComponents;

    public static MainWindow getMainInstance() {
        return instance;
    }

    private void makeActions() {
        var exitStroke = KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_DOWN_MASK);
        var settingsStroke = KeyStroke.getKeyStroke("F2");
        var deleteStroke = KeyStroke.getKeyStroke("DELETE");
        var aboutStroke = KeyStroke.getKeyStroke("F1");

        var actions = new EnumMap<Command, CmdAction>(Command.class);
        actions.put(Command.NEW_PROJECT, new CmdAction(this::newProjectAction, 'N'));
        actions.put(Command.EXIT_APPLICATION, new CmdAction(this::closeWindow, 'E', exitStroke));
        actions.put(Command.SHOW_SETTINGS, new CmdAction(this::showSettings, 'S', settingsStroke));

        actions.put(Command.EDIT_PROJECT, new CmdAction(this::editProjectAction, 'E'));
        actions.put(Command.OPEN_PROJECT_FOLDER, new CmdAction(this::openProjectDirAction, 'O'));
        actions.put(Command.OPEN_BACKUPS_FOLDER, new CmdAction(this::openBackupsDirAction, 'P'));
        actions.put(Command.RESTORE_PROJECT, new CmdAction(this::restoreProjectAction, 'R'));
        actions.put(Command.BACKUP_PROJECT, new CmdAction(this::backupProjectAction, 'B'));
        actions.put(Command.DUPLICATE_PROJECT, new CmdAction(this::duplicateProjectAction, 'D'));
        actions.put(Command.DELETE_PROJECT, new CmdAction(this::deleteProjectAction, 'T', deleteStroke));

        actions.put(Command.SHOW_ABOUT_INFO, new CmdAction(this::showAbout, 'A', aboutStroke));

        defaultCmdAction = actions.get(Command.getDefaultDoubleClickCommand());
        cmdActions = actions;
    }

    private Component makeBottomPanel() {
        var panel = new JPanel();
        var layout = new BoxLayout(panel, BoxLayout.X_AXIS);

        openProjectButton = new JButton("Project folder");
        openBackupsButton = new JButton("Backups folder");
        restoreProjectButton = new JButton("Restore");
        backupProjectButton = new JButton("Backup");

        panel.add(openProjectButton);
        panel.add(Padding.makeButtonSeparator());
        panel.add(openBackupsButton);
        panel.add(Box.createHorizontalGlue());
        panel.add(Box.createHorizontalStrut(Padding.VALUE * 4));
        panel.add(restoreProjectButton);
        panel.add(Padding.makeButtonSeparator());
        panel.add(backupProjectButton);

        panel.setLayout(layout);

        return panel;
    }

    private Component makeCenterArea() {
        projectList = new JList<>();
        projectList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        projectList.setLayoutOrientation(JList.VERTICAL);
        projectList.setVisibleRowCount(VISIBLE_ROW_COUNT);

        projectList.setMinimumSize(projectList.getPreferredSize());

        var scrollArea = new JScrollPane(projectList);

        scrollArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        return scrollArea;
    }

    private void makeLayout() {
        var layout = new BorderLayout(0, Padding.HALF_VALUE);
        var panel = new JPanel(layout);

        var center = makeCenterArea();
        var bottom = makeBottomPanel();

        var topBorder = configuration.isMenuEmbedded() ? 0 : Padding.HALF_VALUE;
        var border = BorderFactory.createEmptyBorder(topBorder, Padding.VALUE, Padding.VALUE, Padding.VALUE);

        panel.setBorder(border);

        panel.add(center, BorderLayout.CENTER);
        panel.add(bottom, BorderLayout.PAGE_END);

        setContentPane(panel);
    }

    private void showError(String message, String title) {
        JOptionPane.showMessageDialog(this, message, title, JOptionPane.ERROR_MESSAGE);
    }

    private void tryOpenDir(String dir) {
        var file = new File(dir);
        if (file.exists()) {
            if (file.isDirectory()) {
                try {
                    Desktop.getDesktop().open(file);
                } catch (IOException ignored) {
                    showError("Failed to open directory", "Error");
                }
            } else {
                showError("Path exists but it's not a directory", "Error");
            }
        } else {
            var result = JOptionPane.showConfirmDialog(
                    this, "The directory does not exist. Would you like to create it?", "Attention",
                    JOptionPane.YES_NO_OPTION
            );
            if (result != JOptionPane.YES_OPTION) {
                return;
            }

            try {
                Files.createDirectories(Path.of(dir));
            } catch (IOException e) {
                showError("Failed to create directory", "Error");
            }

            try {
                Desktop.getDesktop().open(file);
            } catch (IOException ignored) {
                showError("Failed to open directory", "Error");
            }
        }
    }

    private void openBackupsDirAction(ProjectSelection selection) {
        tryOpenDir(selection.project.getBackupDir());
    }

    private void openProjectDirAction(ProjectSelection selection) {
        tryOpenDir(selection.project.getLocalDir());
    }

    private void editProjectAction(ProjectSelection selection) {
        if (ProjectDialog.makeEdit(this, configuration, selection.project)) {
            addProjectToTop(selection.project);
        }
    }

    private void duplicateProjectAction(ProjectSelection selection) {
        var copy = ProjectDialog.makeCopy(this, configuration, selection.project);
        if (copy != null) {
            addProjectToTop(copy);
        }
    }

    private void deleteProjectAction(ProjectSelection selection) {
        var name = selection.project.getName();

        var message = """
                Deleting this project will remove it from the list,\s
                but all project data will remain.\s

                Do you wish to continue?""";

        var result = JOptionPane.showConfirmDialog(this, message, name, JOptionPane.YES_NO_OPTION);
        if (result != JOptionPane.YES_OPTION) {
            return;
        }

        configuration.getProjects().remove(selection.index);
        try {
            configuration.writeToDisc();
        } catch (IOException ex) {
            showError("Failed to write configuration", "Configuration Error");
        }

        getRootPane().grabFocus();

        refreshList(-1);
    }

    private void newProjectAction() {
        var project = ProjectDialog.makeNew(this, configuration);
        if (project != null) {
            addProjectToTop(project);
        }
    }

    private void bindBehaviors() {
        openProjectButton.addActionListener(cmdActions.get(Command.OPEN_PROJECT_FOLDER));
        openBackupsButton.addActionListener(cmdActions.get(Command.OPEN_BACKUPS_FOLDER));
        restoreProjectButton.addActionListener(cmdActions.get(Command.RESTORE_PROJECT));
        backupProjectButton.addActionListener(cmdActions.get(Command.BACKUP_PROJECT));

        projectList.addListSelectionListener(e -> refreshProjectComponents());
        projectList.addMouseListener(new ProjectListMouseAdapter());

        projectComponents = new Component[]{
                openProjectButton,
                openBackupsButton,
                restoreProjectButton,
                backupProjectButton,
                projectMenu
        };
    }

    private void makePopupMenu() {
        popupMenu = new JPopupMenu();
        addMenuOptionsTo(popupMenu);
    }

    private void backupProjectAction(ProjectSelection selection) {
        try {
            if (ProjectArchiver.backup(this, selection.project)) {
                addProjectToTop(selection.project);
            }
        } catch (IOException ex) {
            showError(ex.getMessage(), "Error");
        }
    }

    private void restoreProjectAction(ProjectSelection selection) {
        var message = """
                This is a non-reversible operation that will wipe your project \s
                directory. Are you sure you want to remove all content from: \s

                %s ?

                Make sure there are no active processes using these files, \s
                or restoring might fail and you will have to try again later. \s"""
                .formatted(selection.project.getLocalDir());

        var result = JOptionPane.showConfirmDialog(
                this,
                message,
                "Here be dragons!!!",
                JOptionPane.YES_NO_OPTION
        );
        if (result != JOptionPane.YES_OPTION) {
            return;
        }

        try {
            if (ProjectArchiver.restore(this, selection.project)) {
                addProjectToTop(selection.project);
            }
        } catch (IOException ex) {
            showError(ex.getMessage(), "Error");

        }
    }

    private void addProjectToTop(Project entry) {
        if (entry == null) {
            return;
        }

        var projects = configuration.getProjects();
        var index = projects.indexOf(entry);

        if (index != -1) {
            projects.remove(index);
        }

        projects.add(0, entry);
        refreshList(0);

        try {
            configuration.writeToDisc();
        } catch (IOException ignored) {
            // ignored
        }
    }

    private JMenuItem makeMenuAction(Command command) {
        var action = cmdActions.get(command);

        var menuItem = new JMenuItem(command.toShortString(), action.getMnemonic());

        menuItem.addActionListener(action);
        menuItem.setAccelerator(action.getAccelerator());

        return menuItem;
    }

    private JMenu makeFileMenu() {
        var menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);

        menu.add(makeMenuAction(Command.NEW_PROJECT));
        menu.add(new JSeparator());
        menu.add(makeMenuAction(Command.SHOW_SETTINGS));
        menu.add(new JSeparator());
        menu.add(makeMenuAction(Command.EXIT_APPLICATION));

        return menu;
    }

    private JMenu makeProjectMenu() {
        var menu = new JMenu("Project");
        menu.setMnemonic(KeyEvent.VK_P);
        addMenuOptionsTo(menu);
        projectMenu = menu;

        return projectMenu;
    }

    private void addMenuOptionsTo(JComponent menu) {
        menu.add(makeMenuAction(Command.EDIT_PROJECT));
        menu.add(makeMenuAction(Command.DUPLICATE_PROJECT));
        menu.add(makeMenuAction(Command.DELETE_PROJECT));
        menu.add(new JSeparator());
        menu.add(makeMenuAction(Command.OPEN_PROJECT_FOLDER));
        menu.add(makeMenuAction(Command.OPEN_BACKUPS_FOLDER));
        menu.add(new JSeparator());
        menu.add(makeMenuAction(Command.RESTORE_PROJECT));
        menu.add(makeMenuAction(Command.BACKUP_PROJECT));
    }

    private JMenu makeHelpMenu() {
        var menu = new JMenu("Help");
        menu.setMnemonic(KeyEvent.VK_H);

        menu.add(makeMenuAction(Command.SHOW_ABOUT_INFO));

        return menu;
    }

    private void makeMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        menuBar.setBorder(BorderFactory.createEmptyBorder());

        menuBar.add(makeFileMenu());
        menuBar.add(makeProjectMenu());
        menuBar.add(makeHelpMenu());

        setJMenuBar(menuBar);
    }

    public static MainWindow createNewInstance(Configuration configuration) {
        ThemeManager.setLookAndFeel(configuration);

        // We need to create a new window whenever we change the theme in order for the change to take effect.
        // The old instance will then need to be destroyed, because we should have only one main window.

        var newInstance = new MainWindow(configuration);
        var previousInstance = instance;
        instance = newInstance;
        instance.addShutdownHook();

        if (previousInstance != null) {
            // Copy settings from the previous instance.

            if (!previousInstance.getSize().equals(previousInstance.getMinimumSize())) {
                // The minimum size is the default but also different for each theme.
                // So when the current size is the same as minimum we choose not to copy it to the new instance,
                // so that we use the default size instead.
                newInstance.setSize(previousInstance.getSize());
            }

            newInstance.setAlwaysOnTop(previousInstance.isAlwaysOnTop());
            newInstance.setExtendedState(previousInstance.getExtendedState()); // Maximized etc
            newInstance.setLocationRelativeTo(previousInstance);

            previousInstance.removeShutdownHook();
            previousInstance.dispatchEvent(new WindowEvent(previousInstance, WindowEvent.WINDOW_CLOSING));
        }

        instance.setVisible(true);

        return instance;
    }

    private MainWindow(Configuration configuration) {
        super("Frontdown");
        this.configuration = configuration;

        setIconImages(Resources.APPLICATION_ICONS);

        makeActions();
        makeLayout();
        makePopupMenu();
        makeMenuBar();

        // Obviously this should happen only after all components are created.
        bindBehaviors();

        // We must refresh any time after bindBehaviours() here, but refreshing before pack()
        // fixes a small visual bug that makes the window slightly smaller than intended.
        refreshList(-1);

        pack();
        setMinimumSize(getSize());

        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        getRootPane().grabFocus();
    }

    @Override
    public void toFront() {
        super.toFront();

        // toFront() doesn't always work, so we use this workaround
        setAlwaysOnTop(true);
        setAlwaysOnTop(false);
    }

    private void refreshList(int selection) {
        var projects = configuration.getProjects();
        var array = new Project[projects.size()];
        projects.toArray(array);
        projectList.setListData(array);

        if (selection >= 0 && selection < projects.size()) {
            projectList.setSelectedIndex(selection);
            projectList.ensureIndexIsVisible(selection);
        } else {
            projectList.clearSelection();
        }

        refreshProjectComponents();
    }

    private record ProjectSelection(Project project, int index) {
    }

    private void showSettings() {
        new SettingsWindow(this, configuration);
    }

    private void showAbout() {
        new AboutWindow(this);
    }

    private void refreshProjectComponents() {
        var value = projectList.getSelectedValue() != null;
        for (var component : projectComponents) {
            component.setEnabled(value);
        }
    }

    private void closeWindow() {
        dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
    }

    private void addShutdownHook() {
        if (shutdownHook != null) {
            return;
        }
        shutdownHook = new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Frontdown.Shutdown();
            }
        };

        addWindowListener(shutdownHook);
    }

    private void removeShutdownHook() {
        removeWindowListener(shutdownHook);
        shutdownHook = null;
    }

    private final class CmdAction implements Runnable, ActionListener {

        private final Runnable action;
        private final int mnemonic;
        private final KeyStroke accelerator;

        public int getMnemonic() {
            return mnemonic;
        }

        public KeyStroke getAccelerator() {
            return accelerator;
        }

        public CmdAction(Runnable action, char mnemonic, KeyStroke accelerator) {
            this.action = action;
            this.accelerator = accelerator;
            this.mnemonic = KeyStroke.getKeyStroke(String.valueOf(Character.toUpperCase(mnemonic))).getKeyCode();
        }

        public CmdAction(Runnable action, char mnemonic) {
            this(action, mnemonic, KeyStroke.getKeyStroke(mnemonic, InputEvent.CTRL_DOWN_MASK));
        }

        public CmdAction(Consumer<ProjectSelection> action, char mnemonic, KeyStroke accelerator) {
            this(() -> wrapAction(action), mnemonic, accelerator);
        }

        public CmdAction(Consumer<ProjectSelection> action, char mnemonic) {
            this(() -> wrapAction(action), mnemonic);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            action.run();
        }

        @Override
        public void run() {
            action.run();
        }
    }

    private void wrapAction(Consumer<ProjectSelection> callable) {
        var project = projectList.getSelectedValue();
        if (project == null) {
            return;
        }
        var index = projectList.getSelectedIndex();
        callable.accept(new ProjectSelection(project, index));
    }

    private final class ProjectListMouseAdapter extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
                cmdActions.getOrDefault(configuration.getDoubleClickCommand(), defaultCmdAction).run();
                return;
            }

            showListPopupMenu(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            showListPopupMenu(e);
        }

        @Override
        public void mousePressed(MouseEvent e) {
            showListPopupMenu(e);
        }

        private void showListPopupMenu(MouseEvent e) {
            if (!e.isPopupTrigger()) {
                return;
            }
            var preferredHeight = projectList.getPreferredSize().height;
            var point = e.getPoint();
            if (point.y < 0 || point.y > preferredHeight) {
                return;
            }
            var row = projectList.locationToIndex(e.getPoint());

            projectList.setSelectedIndex(row);

            popupMenu.show(projectList, e.getX(), e.getY());
        }
    }
}
