package com.zoodinger.frontdown;

import com.formdev.flatlaf.FlatDarculaLaf;
import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatIntelliJLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.util.SystemInfo;

import javax.swing.*;
import java.util.*;
import java.util.stream.Stream;


public final class ThemeManager {
    private static final Map<String, UIManager.LookAndFeelInfo> themes;

    private static final List<String> themeNames;

    private static final UIManager.LookAndFeelInfo defaultTheme;

    public static String getDefaultTheme() {
        return defaultTheme.getName();
    }

    public static boolean isFlatLaf(String theme) {
        var info = themes.getOrDefault(theme, null);
        for (var flatlafInfo : flatlafInfos) {
            if (flatlafInfo == info) {
                return true;
            }
        }
        return false;
    }

    private static final UIManager.LookAndFeelInfo[] flatlafInfos;

    static {
        themes = new HashMap<>();

        flatlafInfos = new UIManager.LookAndFeelInfo[]{
                defaultTheme = new UIManager.LookAndFeelInfo("Light", FlatLightLaf.class.getName()),
                new UIManager.LookAndFeelInfo("Dark", FlatDarkLaf.class.getName()),
                new UIManager.LookAndFeelInfo("IntelliJ", FlatIntelliJLaf.class.getName()),
                new UIManager.LookAndFeelInfo("Darcula", FlatDarculaLaf.class.getName())
        };

        var installedInfos = UIManager.getInstalledLookAndFeels();

        var infos = Stream.concat(Arrays.stream(flatlafInfos), Arrays.stream(installedInfos)).toList();
        var names = new ArrayList<String>(infos.size());

        for (var info : infos) {
            if (themes.containsKey(info.getName())) {
                continue;
            }

            themes.put(info.getName(), info);
            names.add(info.getName());
        }

        // Flatlaf themes should be on top
        Collections.sort(names.subList(flatlafInfos.length, names.size()));

        themeNames = Collections.unmodifiableList(names);
    }

    public static void setDefaultTheme() {
        apply(defaultTheme.getClassName(), true);
    }

    public static void setLookAndFeel(Configuration configuration) {
        if (configuration == null) {
            setDefaultTheme();
            return;
        }

        var theme = themes.getOrDefault(configuration.getTheme(), null);
        if (theme == null) {
            theme = defaultTheme;
            configuration.setTheme(theme.getName());
            configuration.setIsMenuEmbedded(systemSupportsEmbeddedMenu());
        }
        apply(theme.getClassName(), configuration.isMenuEmbedded());
    }

    private ThemeManager() {

    }

    public static List<String> getThemes() {
        return themeNames;
    }

    private static void apply(String className, boolean isEmbedded) {
        if (systemSupportsEmbeddedMenu()) {
            System.setProperty("flatlaf.menuBarEmbedded", String.valueOf(isEmbedded));
        }

        try {
            UIManager.setLookAndFeel(className);
        } catch (Exception ignored) {

        }

        UIManager.put("OptionPane.border", Padding.makeBorder(Padding.VALUE));
        UIManager.put("OptionPane.buttonPadding", Padding.HALF_VALUE);

        // For native theme under windows
        UIManager.getDefaults().put("TextArea.font", UIManager.getFont("TextField.font"));
    }

    public static boolean systemSupportsEmbeddedMenu() {
        return SystemInfo.isWindows_10_orLater;
    }
}
