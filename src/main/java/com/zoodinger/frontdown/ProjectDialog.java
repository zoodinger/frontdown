package com.zoodinger.frontdown;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.List;
import java.util.Arrays;
import java.util.Objects;

public class ProjectDialog extends JDialog {
    private final Configuration configuration;
    private final Project project;

    private JTextField projectNameText;
    private JTextField localDirText;
    private JTextField backupDirText;
    private JButton cancelButton;
    private JButton okButton;
    private JCheckBox excludedHiddenCheck;
    private JTextArea excludedFilesText;

    private boolean isAccepted;

    private Component makeBrowsableDirectoryPanel(JTextField field) {
        var panel = new JPanel();
        var borderLayout = new BorderLayout(Padding.HALF_VALUE, 0);
        panel.setLayout(borderLayout);

        var button = new JButton("Browse");

        panel.add(field, BorderLayout.CENTER);
        panel.add(button, BorderLayout.LINE_END);

        button.addActionListener(e -> {
            var chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setCurrentDirectory(new File(field.getText()));

            var result = chooser.showDialog(this, "Select Folder");

            if (result == JFileChooser.APPROVE_OPTION) {
                field.setText(chooser.getSelectedFile().getAbsolutePath());
            }
        });

        return panel;
    }

    private Component makeBottomPanel() {
        var bottomControls = FormBuilder.makeBottomPanel();
        var bottom = bottomControls.getPanel();
        cancelButton = bottomControls.getCancelButton();
        okButton = bottomControls.getOkButton();
        return bottom;
    }

    private void makeLayout() {
        var layout = new BorderLayout(0, 0);
        var panel = new JPanel(layout);

        var center = makeTopForm();
        var bottom = makeBottomPanel();

        panel.setBorder(Padding.makeBorder(Padding.VALUE));

        panel.add(center, BorderLayout.CENTER);
        panel.add(bottom, BorderLayout.PAGE_END);

        setContentPane(panel);
    }

    private Component makeExcludedFilesPanel() {
        var panel = new JPanel();
        var layout = new BorderLayout(0, Padding.HALF_VALUE);
        panel.setLayout(layout);

        excludedFilesText = new JTextArea(4, 1);
        var scrollArea = new JScrollPane(excludedFilesText);
        scrollArea.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollArea.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        panel.add(scrollArea, BorderLayout.CENTER);

        excludedHiddenCheck = new JCheckBox("Exclude hidden files and folders");
        var checkPanel = new JPanel();
        // We put the checkbox a box with a horizontal glue so that it won't stretch along the x axis
        var boxed = new BoxLayout(checkPanel, BoxLayout.X_AXIS);
        checkPanel.add(excludedHiddenCheck);
        checkPanel.add(Box.createHorizontalGlue());
        checkPanel.setLayout(boxed);

        panel.add(checkPanel, BorderLayout.PAGE_END);

        return panel;
    }

    private Component makeTopForm() {
        projectNameText = new JTextField(Padding.WIDE_TEXT);
        localDirText = new JTextField();
        backupDirText = new JTextField();
        excludedHiddenCheck = new JCheckBox("Exclude hidden files and folders");

        var browsableLocalDir = makeBrowsableDirectoryPanel(localDirText);
        var browsablebackupDir = makeBrowsableDirectoryPanel(backupDirText);

        // projectNameText can have a different height than the browsable fields because
        // their browse button might affect their size (depending on the theme). We make
        // sure the first field has the same size as those, for the sake of consistency.
        var size = projectNameText.getPreferredSize();
        size.height = browsableLocalDir.getPreferredSize().height;
        projectNameText.setPreferredSize(size);

        var excludedFilesPanel = makeExcludedFilesPanel();

        return new FormBuilder()
                .setGaps(Padding.VALUE, Padding.VALUE)
                .setLabelAlignment(FormBuilder.LabelAlignmentX.RIGHT, FormBuilder.LabelAlignmentY.MINIMUM_ROW)
                .add("Project name", projectNameText)
                .add("Project directory", browsableLocalDir)
                .add("Backup directory", browsablebackupDir)
                .add("Excluded files", excludedFilesPanel, FormBuilder.Stretch.XY)
                .build();
    }

    private ProjectDialog(JFrame parent, String title, Configuration config, Project project) {
        super(parent, title, true);

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        configuration = config;
        this.project = Objects.requireNonNullElseGet(project, Project::new);

        makeLayout();
        bindBehaviors();

        pack();

        setLocationRelativeTo(parent);

        setMinimumSize(getSize());

        populateFields();

        setVisible(true);
    }

    public static boolean makeEdit(JFrame parent, Configuration config, Project project) {
        return new ProjectDialog(parent, "Edit project (%s)".formatted(project.getName()), config, project).isAccepted;
    }

    public static Project makeCopy(JFrame parent, Configuration config, Project project) {
        var clone = project.clone();
        clone.setName("");
        var dialog = new ProjectDialog(parent, "Duplicate project (%s)".formatted(project.getName()), config, clone);

        if (dialog.isAccepted) {
            return dialog.project;
        } else {
            return null;
        }
    }

    public static Project makeNew(JFrame parent, Configuration config) {
        var dialog = new ProjectDialog(parent, "New project", config, null);
        if (dialog.isAccepted) {
            return dialog.project;
        } else {
            return null;
        }
    }

    private void onCancel() {
        dispose();
    }

    private void bindBehaviors() {
        getRootPane().setDefaultButton(okButton);

        okButton.addActionListener(e -> {
            try {
                validateAndApplyChanges();
                isAccepted = true;
            } catch (ValidationException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage(), "", JOptionPane.WARNING_MESSAGE);
            }

            if (isAccepted) {
                dispose();
            }
        });

        FormBuilder.setupCancelBehavior(this, cancelButton, this::onCancel);

        backupDirText.addFocusListener(new FocusLostListener(this::sanitizeBackupDirText));
        localDirText.addFocusListener(new FocusLostListener(this::sanitizeLocalDirText));
        excludedFilesText.addFocusListener(new FocusLostListener(this::sanitizeExcludedFilesText));
        projectNameText.addFocusListener(new FocusLostListener(this::sanitizeProjectName));
    }

    private record FocusLostListener(Runnable action) implements FocusListener {
        @Override
        public void focusGained(FocusEvent e) {
        }

        @Override
        public void focusLost(FocusEvent e) {
            action.run();
        }
    }

    private void populateFields() {
        projectNameText.setText(project.getName());
        localDirText.setText(project.getLocalDir());
        backupDirText.setText(project.getBackupDir());
        excludedFilesText.setText(String.join("\n", project.getExcludedFiles()));
        excludedHiddenCheck.setSelected(project.isExcludeHidden());
    }

    private String sanitizePath(String pathStr) {
        var path = PathUtils.getPath(pathStr);
        if (path == null) {
            // If path is invalid then we get null, which means we should keep it as-is
            // The user will receive an error later so they can correct their path
            return pathStr;
        }
        path = path.normalize();
        pathStr = path.toString();
        if (pathStr.equals("")) {
            // If path is empty then we keep it this way.
            // The user will receive an error later so they can correct their path.
            return "";
        }

        if (!path.isAbsolute()) {
            try {
                path = Path.of(PathUtils.userHomeDir(), pathStr).toAbsolutePath();
            } catch (InvalidPathException | NullPointerException ignored) {
                // This should never happen unless userHomeDir is invalid for some reason.
                return pathStr;
            }
        }
        return path.normalize().toString();
    }

    private void sanitizeLocalDirText() {
        var path = sanitizePath(localDirText.getText());
        if (path != null) {
            localDirText.setText(path);
        }
    }

    private void sanitizeBackupDirText() {
        var path = sanitizePath(backupDirText.getText());
        if (path != null) {
            backupDirText.setText(path);
        }
    }

    private void sanitizeExcludedFilesText() {
        var lines = getValidExcludedFilePatterns();
        excludedFilesText.setText(String.join("\n", lines));
    }

    private List<String> getValidExcludedFilePatterns() {
        var excludedStr = excludedFilesText.getText();
        String INVALID_PATTERN_CHARS_REGEX = "[\\[\\]{}\"'|;\t,!]";
        excludedStr = excludedStr.replaceAll(INVALID_PATTERN_CHARS_REGEX, "");
        return Arrays.stream(excludedStr.split("[\r\n]"))
                .map(String::trim)
                .filter(e -> !e.isEmpty())
                .distinct() // we don't collect to set because we want to preserve the order
                .toList();
    }

    private void sanitizeProjectName() {
        projectNameText.setText(projectNameText.getText().trim());
    }

    private void sanitizeAllFields() {
        sanitizeProjectName();
        sanitizeLocalDirText();
        sanitizeBackupDirText();
        sanitizeExcludedFilesText();
    }

    private Path validateDirectoryPath(String directory, String directoryName) throws ValidationException {
        if (directory.isBlank()) {
            throw new ValidationException("%s location cannot be empty".formatted(directoryName));
        }
        var path = PathUtils.getPath(directory);
        if (path == null) {
            throw new ValidationException("%s path is invalid".formatted(directoryName));
        }

        return path;
    }

    private void validateProjectname(String name) throws ValidationException {
        if (name.isBlank()) {
            throw new ValidationException("Project name cannot be empty");
        }

        for (var p : configuration.getProjects()) {
            if (p.getName().equals(name)) {
                if (p != project) {
                    throw new ValidationException("Another project with this name already exists");
                }
                break;
            }
        }
    }

    private void validateAndApplyChanges() throws ValidationException {
        sanitizeAllFields();

        var newName = projectNameText.getText().trim();
        var newLocalDir = localDirText.getText().trim();
        var newBackupDir = backupDirText.getText().trim();
        var newExcludedFiles = getValidExcludedFilePatterns();
        var newExcludedHidden = excludedHiddenCheck.isSelected();

        validateProjectname(newName);

        var localDirPath = validateDirectoryPath(newLocalDir, "Project");
        var backupDirPath = validateDirectoryPath(newBackupDir, "Backup");
        if (localDirPath.startsWith(backupDirPath)) {
            throw new ValidationException("Project path cannot be a subdirectory of backup path");
        }
        if (backupDirPath.startsWith(localDirPath)) {
            throw new ValidationException("Backup path cannot be a subdirectory of project path");
        }

        project.setName(newName);
        project.setBackupDir(newBackupDir);
        project.setLocalDir(newLocalDir);
        project.setExcludedFiles(newExcludedFiles);
        project.setExcludeHidden(newExcludedHidden);
    }

    private static class ValidationException extends Exception {
        public ValidationException(String message) {
            super(message);
        }
    }
}
