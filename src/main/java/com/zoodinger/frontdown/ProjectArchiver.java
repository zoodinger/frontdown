package com.zoodinger.frontdown;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CancellationException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ProjectArchiver {
    private final String projectName;

    private final String localStr;
    private final String backupStr;
    private final Path localPath;
    private final Path backupPath;
    private final PathMatcher glob;

    private final FileSystem system;
    private final boolean excludeHidden;

    private final JFrame parentFrame;

    private static final int BUFFER_SIZE = 8192;
    private final byte[] byteBuffer = new byte[BUFFER_SIZE];

    private static final SimpleDateFormat FORMATTER;
    private static final Pattern BACKUP_PATTERN = Pattern.compile("[0-9]{8}_[0-9]{6}\\.zip");

    private ProgressDialog.Worker worker;

    private static final String RESTORE_CANCEL_MESSAGE = """
            Restoration process was cancelled!\s

            The project is now in a corrupted state.\s

            Please try restoring again later.""";

    private static final String RESTORE_ERROR_MESSAGE = """
            Failed to restore project!\s

            The project is now in a corrupted state.\s
            You might need to manually resolve this error and try again.

            Reason:
            """;

    static {
        FORMATTER = new SimpleDateFormat("yyyyMMdd_HHmmss");
        FORMATTER.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    private static String getNewFileName() {
        return FORMATTER.format(new Date()) + ".zip";
    }

    private boolean isValidPattern(String pattern) {
        try {
            system.getPathMatcher("glob:" + pattern);
        } catch (UnsupportedOperationException | IllegalArgumentException e) {
            return false;
        }
        return true;
    }

    public static boolean restore(JFrame parent, Project entry) throws IOException {
        var archiver = new ProjectArchiver(parent, entry);
        return archiver.restore();
    }

    public static boolean backup(JFrame parent, Project entry) throws IOException {
        var archiver = new ProjectArchiver(parent, entry);
        return archiver.backup();
    }

    public ProjectArchiver(JFrame parent, Project entry) {
        parentFrame = parent;
        projectName = entry.getName();

        localPath = PathUtils.getPath(entry.getLocalDir());
        localStr = localPath == null ? null : localPath.toAbsolutePath().toString();

        backupPath = PathUtils.getPath(entry.getBackupDir());
        backupStr = backupPath == null ? null : backupPath.toAbsolutePath().toString();

        var excluded = entry.getExcludedFiles();
        excludeHidden = entry.isExcludeHidden();

        system = FileSystems.getDefault();

        var globPatterns = excluded.stream()
                .map(e -> e.trim().replace("\\", "/"))
                .filter(e -> !e.isEmpty())
                .filter(this::isValidPattern)
                .collect(Collectors.toSet());

        PathMatcher finalGlob = null;
        if (globPatterns.size() > 0) {
            var pattern = String.format("glob:{%s}", String.join(",", globPatterns));
            try {
                finalGlob = system.getPathMatcher(pattern);
            } catch (UnsupportedOperationException | IllegalArgumentException ignored) {
                // ignored
            }
        }
        glob = finalGlob;
    }

    private record PendingFile(File file, Path absPath, Path relativePath) {
    }

    private class ZipFileVisitor extends SimpleFileVisitor<Path> {
        public ArrayList<PendingFile> results = new ArrayList<>();

        private boolean tryAdd(Path path) {
            assert localPath != null;
            var relative = localPath.relativize(path);

            if (glob != null && glob.matches(relative)) {
                return false;
            }

            var file = path.toFile();
            if (!file.exists() || (excludeHidden && file.isHidden())) {
                return false;
            }

            results.add(new PendingFile(file, path, relative));
            return true;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
            return tryAdd(dir) ? FileVisitResult.CONTINUE : FileVisitResult.SKIP_SUBTREE;
        }

        @Override
        public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
            tryAdd(path);
            return FileVisitResult.CONTINUE;
        }
    }

    private void zipFile(ZipOutputStream stream, File file, Path pathInZip) throws Exception {
        if (file.isDirectory()) {
            stream.putNextEntry(new ZipEntry(pathInZip.toString() + "/"));
            stream.closeEntry();
            return;
        }

        try (var fis = new FileInputStream(file)) {
            int length;
            try {
                length = fis.read(byteBuffer);
            } catch (IOException ex) {
                // File is locked (or deleted?), ignore
                return;
            }
            if (length >= 0) {
                var zipEntry = new ZipEntry(pathInZip.toString());
                stream.putNextEntry(zipEntry);
                do {
                    stream.write(byteBuffer, 0, length);
                    stopIfCancelled();
                } while ((length = fis.read(byteBuffer)) >= 0);

            }
        }
    }

    private boolean backup() throws IOException {
        if (localPath == null || backupPath == null || backupStr == null) {
            return false;
        }

        var parentFile = localPath.toFile();
        if (!parentFile.exists() || !parentFile.isDirectory()) {
            throw new IOException("Project directory is empty");
        }

        var newFileName = getNewFileName();
        var newZipFilePath = Path.of(backupStr, newFileName);
        var newZipFile = newZipFilePath.toFile();
        var newTmpFile = new File(newZipFilePath.toAbsolutePath() + ".tmp");

        var success = new boolean[]{false};

        var task = new ProgressDialog.Task() {
            @Override
            public void run() throws Exception {
                ProjectArchiver.this.worker = getWorker();
                worker.showProgress(false);

                touchDirectory(backupPath);

                // First we gather all the entries
                var visitor = new ZipFileVisitor();
                Files.walkFileTree(localPath, visitor);
                var fileCount = visitor.results.size();

                try (var fos = new FileOutputStream(newTmpFile)) {
                    try (var zipOut = new ZipOutputStream(fos)) {

                        worker.setProgress(0);
                        worker.showProgress(true);

                        // We need to skip the first file because that represents the directory itself.
                        // Adding it to the zip file will create an invalid entry and cause the unzipping to fail.
                        for (var i = 1; i < visitor.results.size(); ++i) {
                            var fileToWrite = visitor.results.get(i);
                            worker.setMessage(fileToWrite.relativePath.toString());

                            zipFile(zipOut, fileToWrite.file, fileToWrite.relativePath);

                            stopIfCancelled();

                            worker.setProgressRatio(i / (double) fileCount);
                        }

                        worker.showProgress(false);
                        worker.setMessage("Finalizing...");
                        worker.setCaption("Please wait...");
                    }
                }

                Files.move(newTmpFile.toPath(), newZipFile.toPath());

                worker.showProgress(true);
                worker.setProgress(100);
                worker.setCaption("Done!");
                worker.setMessage("Written files to " + newZipFile.getAbsolutePath());
            }

            @Override
            protected ProgressDialog.Message onCancel() {
                try {
                    Files.delete(newTmpFile.toPath());
                } catch (Exception ignored) {
                }
                newTmpFile.deleteOnExit();

                return null;
            }

            @Override
            protected void onDone() {
                success[0] = true;
            }
        };

        new ProgressDialog(parentFrame, task, "Backing up - " + projectName, "Please wait...", "Calculating...");

        return success[0];
    }

    // Calls deleteFilesRecursively internally to delete contents but does not delete the file itself.
    private void deleteContents(File file) throws IOException {
        if (file == null || !file.exists()) {
            throw new IOException("Failed to create project directory");
        }

        if (!file.isDirectory()) {
            throw new IOException("Project path is not a directory");
        }

        var files = file.listFiles();
        if (files == null) {
            return;
        }

        for (var subFile : files) {
            deleteFilesRecursively(subFile);
        }
    }

    private boolean deleteFilesRecursively(File file) throws IOException {
        stopIfCancelled();

        var path = file.toPath();
        var relative = localPath.relativize(path);

        boolean canDelete = true;

        // File needs to be skipped because it matches the pattern
        if (glob != null && glob.matches(relative)) {
            return false;
        }

        // If path is symbolic we'll treat it like a normal file
        if (!Files.isSymbolicLink(path)) {
            var files = file.listFiles();
            if (files != null) {
                for (var f : files) {
                    if (!deleteFilesRecursively(f)) {
                        canDelete = false;
                    }
                }
            }
        }

        if (canDelete) {
            if (worker != null) {
                worker.setMessage(file.getAbsolutePath());
            }
            try {
                Files.delete(file.toPath());
            } catch (IOException e) {
                throw new IOException("Failed to delete file " + e.getMessage());
            }
        }

        return true;
    }

    int countEntriesInZipFile(String path) throws IOException {
        var count = 0;
        var file = new File(path);
        try (var zipFile = new ZipFile(file)) {
            var entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                entries.nextElement();
                ++count;
            }
        } catch (Exception e) {
            throw new IOException("Failed to read zip file '%s'".formatted(path));
        }
        return count;
    }

    private boolean tryUnzipNextEntry(ZipInputStream stream) throws IOException {
        var entry = stream.getNextEntry();
        if (entry == null) {
            return false;
        }

        var resolvedPath = localPath.resolve(entry.getName()).normalize();
        if (!resolvedPath.startsWith(localPath)) {
            throw new IOException("Invalid zip entry");
        }

        if (worker != null) {
            worker.setMessage(entry.getName());
        }
        try {
            if (entry.isDirectory()) {
                Files.createDirectories(resolvedPath);
            } else {
                Files.createDirectories(resolvedPath.getParent());
                int length;
                try (var out = new FileOutputStream(resolvedPath.toAbsolutePath().toString())) {
                    while ((length = stream.read(byteBuffer)) >= 0) {
                        if (worker.isCancelled()) {

                            // Delete incomplete file, don't wait for it to finish
                            out.close();
                            try {
                                Files.delete(resolvedPath);
                            } catch (Exception ignored) {
                                // ignored
                            }

                            stopIfCancelled();
                        }

                        out.write(byteBuffer, 0, length);
                    }
                }
            }
        } catch (IOException e) {
            throw new IOException("Failed to write file " + entry.getName());
        }

        return true;
    }


    private boolean restore() throws IOException {
        var backupFiles = getBackupFiles();
        if (backupFiles.size() == 0) {
            throw new IOException("No backups found in the backup directory");
        }

        if (localPath == null) {
            return false;
        }

        var success = new boolean[]{false};

        var task = new ProgressDialog.Task() {
            @Override
            void run() throws Exception {
                ProjectArchiver.this.worker = getWorker();
                worker.showProgress(false);

                var backup = backupFiles.get(backupFiles.size() - 1);
                var localDir = touchDirectory(localPath);

                worker.showProgress(false);
                worker.setCaption("Deleting files...");

                deleteContents(localDir);

                stopIfCancelled();

                worker.setCaption("Calculating...");

                var totalEntries = countEntriesInZipFile(backup);

                worker.setCaption("Writing files...");
                worker.setProgress(0);
                worker.showProgress(true);

                stopIfCancelled();

                try (var fos = new FileInputStream(backup)) {
                    try (var zipIn = new ZipInputStream(fos)) {
                        for (var i = 0; tryUnzipNextEntry(zipIn); ++i) {
                            worker.setProgressRatio(i / (double) totalEntries);
                        }
                    }
                }

                worker.setProgress(100);
                worker.setCaption("Done!");
                worker.setMessage("Written files to " + localPath);
            }

            @Override
            public ProgressDialog.Message onCancel() {
                return ProgressDialog.Message.makeWarning(RESTORE_CANCEL_MESSAGE);
            }

            @Override
            public ProgressDialog.Message onError(Exception exception) {
                var message = RESTORE_ERROR_MESSAGE + exception.getMessage();
                return ProgressDialog.Message.makeError(message);
            }

            @Override
            protected void onDone() {
                success[0] = true;
            }
        };

        new ProgressDialog(parentFrame, task, "Restoring - " + projectName, "Please wait...", "Calculating...");

        return success[0];
    }

    private void stopIfCancelled() {
        if (worker != null && worker.isCancelled()) {
            throw new CancellationException();
        }
    }

    private File touchDirectory(Path path) throws IOException {
        var file = path.toFile();
        if (file.exists()) {
            if (file.isFile()) {
                throw new IOException("Project path is not a directory");
            }
        } else if (!file.mkdirs()) {
            throw new IOException("Could not create project directory");
        }

        return file;
    }

    public List<String> getBackupFiles() {
        var dir = new File(backupStr);
        try {
            var files = dir.listFiles(e -> BACKUP_PATTERN.matcher(e.getName()).matches());
            if (files == null) {
                return List.of();
            }
            return Arrays.stream(files)
                    .filter(File::isFile)
                    .filter(File::canRead)
                    .map(File::getAbsolutePath)
                    .sorted()
                    .toList();
        } catch (Exception e) {
            e.printStackTrace();
            return List.of();
        }
    }
}
