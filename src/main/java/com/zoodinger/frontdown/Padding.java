package com.zoodinger.frontdown;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public final class Padding {
    public static final int VALUE = 8;
    public static final int HALF_VALUE = VALUE / 2;

    public static Component makeButtonSeparator() {
        return Box.createHorizontalStrut(HALF_VALUE);
    }

    public static Border makeBorder(int value) {
        return BorderFactory.createEmptyBorder(value, value, value, value);
    }

    public static final String WIDE_TEXT =
            "Placeholder text to help create a wide window when calling the pack() method";

    private Padding() {
    }
}
