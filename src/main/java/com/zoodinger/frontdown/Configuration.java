package com.zoodinger.frontdown;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public final class Configuration {
    @SerializedName("projects")
    private ArrayList<Project> projects = new ArrayList<>();

    @SerializedName("theme")
    private String theme = ThemeManager.getDefaultTheme();

    @SerializedName("double_click_action")
    private Command doubleClickCommand = Command.getDefaultDoubleClickCommand();

    @SerializedName("embed_menu")
    private boolean isMenuEmbedded = ThemeManager.systemSupportsEmbeddedMenu();

    @SerializedName("version")
    private String savedVersion;

    public List<Project> getProjects() {
        return projects;
    }

    public String getTheme() {
        return theme == null ? ThemeManager.getDefaultTheme() : theme;
    }

    public boolean getIsMenuEmbedded() {
        return isMenuEmbedded;
    }

    public String getSavedVersion() {
        return savedVersion;
    }

    public boolean isMenuEmbedded() {
        return ThemeManager.systemSupportsEmbeddedMenu()
                && ThemeManager.isFlatLaf(getTheme())
                && getIsMenuEmbedded();
    }

    public Command getDoubleClickCommand() {
        if (doubleClickCommand == null || !doubleClickCommand.isDoubleClickCommand()) {
            doubleClickCommand = Command.getDefaultDoubleClickCommand();
        }
        return doubleClickCommand;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setIsMenuEmbedded(boolean value) {
        isMenuEmbedded = value;
    }

    public void setDoubleClickCommand(Command command) {
        doubleClickCommand = command.isDoubleClickCommand() ? command : Command.getDefaultDoubleClickCommand();
    }

    public static Configuration readFromDisk() throws IOException {
        var filename = PathUtils.getConfigFilename();
        Configuration configuration;
        try {
            var reader = new JsonReader(new FileReader(filename));
            configuration = new Gson().fromJson(reader, Configuration.class);
        } catch (FileNotFoundException ignored) {
            new Configuration().writeToDisc();
            try (var reader = new JsonReader(new FileReader(filename))) {
                configuration = new Gson().fromJson(reader, Configuration.class);
            }
        }

        return configuration;
    }

    public void writeToDisc() throws IOException {
        savedVersion = Resources.getVersion();

        var gson = new GsonBuilder().setPrettyPrinting().create();
        Files.createDirectories(PathUtils.getConfigPath());

        try (var writer = new FileWriter(PathUtils.getConfigFilename())) {
            gson.toJson(this, writer);
        }
    }
}
