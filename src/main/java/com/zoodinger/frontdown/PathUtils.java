package com.zoodinger.frontdown;

import com.formdev.flatlaf.util.SystemInfo;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;

public final class PathUtils {
    private static final String configLocation;
    private static final Path configPath;
    private static final String userHome;

    static {
        userHome = System.getProperty("user.home");

        String appdata;
        
        if (SystemInfo.isWindows) {
            appdata = Path.of(System.getenv("APPDATA"), "..", "Local").toString();
        } else {
            var homeDir = System.getProperty("user.home");
            appdata = Path.of(homeDir, ".local").toString();
        }
        configPath = Path.of(appdata, "frontdown").toAbsolutePath().normalize();
        configLocation = Path.of(configPath.toString(), "config.json").toAbsolutePath().toString();
    }

    public static String userHomeDir() {
        return userHome;
    }

    public static Path getPath(String path) {
        if (path == null) {
            return null;
        }
        try {
            return Path.of(path);
        } catch (InvalidPathException ignored) {
            return null;
        }
    }

    public static String getConfigFilename() {
        return configLocation;
    }

    public static Path getConfigPath() {
        return configPath;
    }

    private PathUtils() {

    }
}
