package com.zoodinger.frontdown;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.util.Objects;

public class SettingsWindow extends JDialog {
    private final Configuration configuration;

    private JComboBox<String> themeCombo;
    private JComboBox<Command> actionCombo;
    private JCheckBox embedCheckbox;
    private JButton okButton;
    private JButton cancelButton;
    private JButton defaultsButton;

    private void onCancel() {
        dispose();
    }

    public SettingsWindow(JFrame owner, Configuration configuration) {
        super(owner, "Settings", true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.configuration = configuration;

        makeLayout();
        bindBehaviours();

        populateControls();

        pack();

        setResizable(false);
        setLocationRelativeTo(owner);

        getRootPane().setDefaultButton(okButton);

        setVisible(true);
    }

    private void populateControls() {
        themeCombo.setSelectedItem(configuration.getTheme());
        if (ThemeManager.systemSupportsEmbeddedMenu()) {
            embedCheckbox.setSelected(configuration.getIsMenuEmbedded());
        }
        actionCombo.setSelectedItem(configuration.getDoubleClickCommand());
    }

    private void saveChanges() {
        var systemSupportsEmbeddedMenu = ThemeManager.systemSupportsEmbeddedMenu();

        var newTheme = ((String) Objects.requireNonNull(themeCombo.getSelectedItem()));
        var newAction = ((Command) Objects.requireNonNull(actionCombo.getSelectedItem()));
        var newEmbedded = systemSupportsEmbeddedMenu && embedCheckbox.isSelected();

        // Determine whether the UI needs to be reloaded
        var changeThemeCondition1 = !newTheme.equals(configuration.getTheme());
        var changeThemeCondition2 = systemSupportsEmbeddedMenu
                && ThemeManager.isFlatLaf(newTheme)
                && newEmbedded != configuration.getIsMenuEmbedded();

        configuration.setTheme(newTheme);
        configuration.setDoubleClickCommand(newAction);
        if (systemSupportsEmbeddedMenu) {
            configuration.setIsMenuEmbedded(newEmbedded);
        }

        try {
            configuration.writeToDisc();
        } catch (Exception ignored) {
        }

        if (changeThemeCondition1 || changeThemeCondition2) {
            MainWindow.createNewInstance(configuration);
        }
    }

    private void bindBehaviours() {
        okButton.addActionListener(e -> {
            saveChanges();
            dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        });

        FormBuilder.setupCancelBehavior(this, cancelButton, this::onCancel);

        if (ThemeManager.systemSupportsEmbeddedMenu()) {
            themeCombo.addItemListener(e -> {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    var theme = (String) e.getItem();

                    var supportsOption = ThemeManager.isFlatLaf(theme);

                    embedCheckbox.setEnabled(supportsOption);
                }
            });
        }

        defaultsButton.addActionListener(e -> resetDefaults());
    }

    private void resetDefaults() {
        themeCombo.setSelectedItem(ThemeManager.getDefaultTheme());
        actionCombo.setSelectedItem(Command.getDefaultDoubleClickCommand());
        if (ThemeManager.systemSupportsEmbeddedMenu()) {
            embedCheckbox.setSelected(true);
        }
    }

    private Component makeBottom() {
        var bottomControls = FormBuilder.makeBottomPanel();
        var buttonPanel = bottomControls.getPanel();
        okButton = bottomControls.getOkButton();
        cancelButton = bottomControls.getCancelButton();

        defaultsButton = new JButton("Defaults");

        var panel = new JPanel();
        var layout = new BoxLayout(panel, BoxLayout.X_AXIS);
        panel.add(defaultsButton);
        panel.add(Box.createHorizontalStrut(Padding.VALUE * 2));
        panel.add(buttonPanel);
        panel.setLayout(layout);

        defaultsButton.setMnemonic(KeyEvent.VK_D);

        return panel;
    }

    private Component makeComboArea(JComboBox<?> combo, Component right) {
        var size = combo.getPreferredSize();
        size.width += Padding.VALUE;
        combo.setPreferredSize(size);

        var panel = new JPanel();
        var layout = new BorderLayout(Padding.HALF_VALUE, 0);
        panel.setLayout(layout);

        if (right == null) {
            right = Box.createHorizontalGlue();
        }

        panel.add(combo, BorderLayout.LINE_START);
        panel.add(right, BorderLayout.CENTER);

        return panel;
    }

    private Component makeThemeArea() {
        themeCombo = new JComboBox<>();
        for (var theme : ThemeManager.getThemes()) {
            themeCombo.addItem(theme);
        }

        if (!ThemeManager.systemSupportsEmbeddedMenu()) {
            return makeComboArea(themeCombo, null);
        }

        embedCheckbox = new JCheckBox("Embed menu to title");
        return makeComboArea(themeCombo, embedCheckbox);
    }

    private Component makeActionArea() {
        actionCombo = new JComboBox<>();
        for (var action : Command.getDoubleClickValues()) {
            actionCombo.addItem(action);
        }

        return makeComboArea(actionCombo, null);
    }

    private Component makeTop() {
        var themePanel = makeThemeArea();
        var actionPanel = makeActionArea();

        return new FormBuilder()
                .setGaps(Padding.VALUE, Padding.VALUE)
                .setLabelAlignment(FormBuilder.LabelAlignmentX.RIGHT, FormBuilder.LabelAlignmentY.MINIMUM_ROW)
                .add("Look and feel", themePanel)
                .add("Double click action", actionPanel)
                .build();
    }

    private void makeLayout() {
        var layout = new BorderLayout(0, Padding.VALUE);
        var panel = new JPanel();
        panel.setBorder(Padding.makeBorder(Padding.VALUE));

        panel.setLayout(layout);

        panel.add(makeTop(), BorderLayout.PAGE_START);
        panel.add(makeBottom(), BorderLayout.PAGE_END);

        setContentPane(panel);
    }
}
