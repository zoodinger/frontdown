package com.zoodinger.frontdown;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

public final class Resources {
    private Resources() {
    }

    public static final List<BufferedImage> APPLICATION_ICONS;
    public static final Icon ABOUT_ICON;

    public static BufferedImage getImage(String name) {
        var stream = ClassLoader.getSystemResourceAsStream(name);
        if (stream == null) {
            return null;
        }
        try {
            return ImageIO.read(stream);
        } catch (IOException e) {
            return null;
        }
    }

    public static String getVersion() {
        return properties.getProperty("version", "");
    }

    public static String getAuthor() {
        return properties.getProperty("author", "");
    }

    private static final Properties properties;

    static {
        APPLICATION_ICONS = Arrays.stream(new int[]{8, 16, 24, 32, 48, 64, 96, 128, 256, 512})
                .mapToObj("icon/%d.png"::formatted)
                .map(Resources::getImage)
                .filter(Objects::nonNull)
                .toList();

        ABOUT_ICON = new ImageIcon(Objects.requireNonNull(getImage("icon/64.png")));

        properties = new Properties();
        try (var stream = ClassLoader.getSystemResourceAsStream("frontdown.properties")) {
            properties.load(stream);
        } catch (IOException | NullPointerException ignored) {
        }
    }

}
