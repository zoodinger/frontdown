package com.zoodinger.frontdown;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;

public enum Command {
    // Standard project actions
    EDIT_PROJECT,
    OPEN_PROJECT_FOLDER,
    OPEN_BACKUPS_FOLDER,
    RESTORE_PROJECT,
    BACKUP_PROJECT,

    // Extended project actions
    DUPLICATE_PROJECT,
    DELETE_PROJECT,

    // General actions
    NEW_PROJECT,

    // Miscellanious actions
    SHOW_SETTINGS,
    SHOW_ABOUT_INFO,
    EXIT_APPLICATION;

    private static final List<Command> doubleClickCommands;
    private static final EnumMap<Command, String> shortStrings;
    private static final EnumSet<Command> doubleClickMap;

    public static List<Command> getDoubleClickValues() {
        return doubleClickCommands;
    }

    static {
        var commands = new Command[]{
                EDIT_PROJECT, OPEN_PROJECT_FOLDER, OPEN_BACKUPS_FOLDER, RESTORE_PROJECT, BACKUP_PROJECT
        };

        doubleClickCommands = List.of(commands);

        doubleClickMap = EnumSet.noneOf(Command.class);
        doubleClickMap.addAll(doubleClickCommands);

        shortStrings = new EnumMap<>(Command.class);
        shortStrings.put(EDIT_PROJECT, "Edit");
        shortStrings.put(RESTORE_PROJECT, "Restore");
        shortStrings.put(BACKUP_PROJECT, "Backup");
        shortStrings.put(DUPLICATE_PROJECT, "Duplicate");
        shortStrings.put(DELETE_PROJECT, "Delete");
        shortStrings.put(SHOW_SETTINGS, "Settings");
        shortStrings.put(SHOW_ABOUT_INFO, "About");
        shortStrings.put(EXIT_APPLICATION, "Exit");

        for (var value : values()) {
            shortStrings.putIfAbsent(value, value.toString());
        }
    }

    public boolean isDoubleClickCommand() {
        return doubleClickMap.contains(this);
    }

    public static Command getDefaultDoubleClickCommand() {
        return EDIT_PROJECT;
    }

    public String toShortString() {
        return shortStrings.get(this);
    }

    @Override
    public String toString() {
        // Converts "SCREAMING_SNAKE_CASE" to "Screaming snake case"

        var str = super.toString();

        var sentence = str.replaceAll("_", " ");
        return sentence.charAt(0) + sentence.substring(1).toLowerCase();
    }
}
