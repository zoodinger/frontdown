package com.zoodinger.frontdown;

import com.formdev.flatlaf.util.SystemInfo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class FormBuilder {
    private int hGap;
    private int vGap;

    private int maxRowHeight;
    private int minRowHeight = Integer.MAX_VALUE;

    private LabelAlignmentX currentAlignmentX = LabelAlignmentX.RIGHT;
    private LabelAlignmentY currentAlignmentY = LabelAlignmentY.CENTER;

    private final ArrayList<BuildEntry> entries = new ArrayList<>();

    private static class BuildEntry {
        JLabel label;
        GridBagConstraints labelConstraints;

        Component component;
        GridBagConstraints componentConstraints;

        Stretch stretch;

        LabelAlignmentY alignmentY;
        int rowHeight;
    }

    private enum VerticalLabelStrategy {
        CENTER,
        VALUE,
        LIKE_ROW,
        MINIMUM_ROW,
        MAXIMUM_ROW,
    }

    public static class LabelAlignmentY {
        private final VerticalLabelStrategy strategy;
        private final int value;

        public static final LabelAlignmentY CENTER = new LabelAlignmentY(VerticalLabelStrategy.CENTER);
        public static final LabelAlignmentY MINIMUM_ROW = new LabelAlignmentY(VerticalLabelStrategy.MINIMUM_ROW);
        public static final LabelAlignmentY MAXIMUM_ROW = new LabelAlignmentY(VerticalLabelStrategy.MAXIMUM_ROW);

        public static LabelAlignmentY makeHeightOfRow(int row) {
            return new LabelAlignmentY(VerticalLabelStrategy.LIKE_ROW, row);
        }

        public static LabelAlignmentY makeHeight(int height) {
            return new LabelAlignmentY(VerticalLabelStrategy.VALUE, height);
        }

        private LabelAlignmentY(VerticalLabelStrategy strategy, int value) {
            this.strategy = strategy;
            this.value = value;
        }

        private LabelAlignmentY(VerticalLabelStrategy strategy) {
            this.strategy = strategy;
            value = 0;
        }
    }

    public FormBuilder setGaps(int hGap, int vGap) {
        this.hGap = hGap;
        this.vGap = vGap;
        return this;
    }

    public enum LabelAlignmentX {
        RIGHT,
        LEFT,
    }

    public enum Stretch {
        NONE,
        X,
        Y,
        XY;

        boolean getX() {
            return switch (this) {
                case Y, NONE -> false;
                case X, XY -> true;
            };
        }

        boolean getY() {
            return switch (this) {
                case X, NONE -> false;
                case Y, XY -> true;
            };
        }
    }

    public FormBuilder setLabelAlignment(LabelAlignmentX alignmentX) {
        currentAlignmentX = alignmentX;
        return this;
    }

    public FormBuilder setLabelAlignment(LabelAlignmentY alignmentY) {
        currentAlignmentY = alignmentY;
        return this;
    }

    public FormBuilder setLabelAlignment(LabelAlignmentX alignmentX, LabelAlignmentY alignmentY) {
        currentAlignmentX = alignmentX;
        currentAlignmentY = alignmentY;
        return this;
    }

    public FormBuilder add(String label, Component component) {
        return add(label, component, Stretch.X);
    }

    public FormBuilder add(String label, Component component, Stretch stretch) {

        var entry = new BuildEntry();

        JLabel labelComponent = null;
        if (label != null && !label.isEmpty()) {
            labelComponent = new JLabel(label);
            labelComponent.setLabelFor(component);
        }

        var row = entries.size();
        var topInset = row == 0 ? 0 : vGap;

        var rowHeight = 0;

        var stretchX = stretch.getX();
        var stretchY = stretch.getY();

        if (labelComponent != null) {
            var leftConstraints = new GridBagConstraints();
            leftConstraints.gridy = row;
            leftConstraints.weighty = stretchY ? 1 : 0;
            leftConstraints.insets = new Insets(topInset, 0, 0, hGap);
            leftConstraints.gridx = 0;
            leftConstraints.weightx = 0;

            rowHeight = labelComponent.getPreferredSize().height;

            entry.label = labelComponent;
            entry.labelConstraints = leftConstraints;
        }

        var constraints = new GridBagConstraints();
        constraints.gridy = row;
        constraints.anchor = GridBagConstraints.LINE_START;
        constraints.insets = new Insets(topInset, 0, 0, 0);
        constraints.gridx = 1;
        constraints.weightx = 1;

        if (stretchY && stretchX) {
            constraints.fill = GridBagConstraints.BOTH;
        } else if (stretchX) {
            constraints.fill = GridBagConstraints.HORIZONTAL;
        } else if (stretchY) {
            constraints.fill = GridBagConstraints.VERTICAL;
        } else {
            constraints.fill = GridBagConstraints.NONE;
        }

        rowHeight = Math.max(rowHeight, component.getPreferredSize().height);

        maxRowHeight = Math.max(maxRowHeight, rowHeight);
        minRowHeight = Math.min(minRowHeight, rowHeight);

        entry.component = component;
        entry.componentConstraints = constraints;
        entry.stretch = stretch;

        entry.rowHeight = rowHeight;
        entry.alignmentY = currentAlignmentY;

        entries.add(entry);

        return this;
    }

    private void makeValueConstraint(BuildEntry entry, int value) {
        entry.labelConstraints.anchor = switch (currentAlignmentX) {
            case LEFT -> GridBagConstraints.FIRST_LINE_START;
            case RIGHT -> GridBagConstraints.FIRST_LINE_END;
        };

        var size = entry.label.getPreferredSize();
        size.height = Math.min(entry.rowHeight, Math.max(size.height, value));
        entry.label.setPreferredSize(size);
        entry.label.setMinimumSize(size);
        entry.label.setMaximumSize(size);
        entry.label.setVerticalAlignment(JLabel.CENTER);
    }

    private void makeVerticalConstraint(BuildEntry entry, LabelAlignmentY alignment) {
        switch (alignment.strategy) {
            case CENTER -> entry.labelConstraints.anchor = switch (currentAlignmentX) {
                case LEFT -> GridBagConstraints.LINE_START;
                case RIGHT -> GridBagConstraints.LINE_END;
            };
            case VALUE -> makeValueConstraint(entry, alignment.value);
            case MINIMUM_ROW -> makeValueConstraint(entry, minRowHeight);
            case MAXIMUM_ROW -> makeValueConstraint(entry, maxRowHeight);
            case LIKE_ROW -> makeValueConstraint(entry, entries.get(alignment.value).rowHeight);
        }
    }

    public JPanel build(ArrayList<ComponentPair> grabComponents) {
        var panel = build();

        grabComponents.clear();
        for (var entry : entries) {
            grabComponents.add(new ComponentPair(entry.label, entry.component));
        }

        return panel;
    }

    public JPanel build() {
        var panel = new JPanel(new GridBagLayout());

        minRowHeight = Math.min(maxRowHeight, minRowHeight);

        for (var entry : entries) {
            if (entry.label != null) {
                makeVerticalConstraint(entry, entry.alignmentY);
                panel.add(entry.label, entry.labelConstraints);
            }
            panel.add(entry.component, entry.componentConstraints);
        }

        return panel;
    }

    public static class BottomPanelControls {
        private final JPanel panel;
        private final JButton okButton;
        private final JButton cancelButton;

        private BottomPanelControls(JPanel panel, JButton okButton, JButton cancelButton) {
            this.panel = panel;
            this.okButton = okButton;
            this.cancelButton = cancelButton;
        }

        public JPanel getPanel() {
            return panel;
        }

        public JButton getOkButton() {
            return okButton;
        }

        public JButton getCancelButton() {
            return cancelButton;
        }
    }

    public record ComponentPair(JLabel label, Component component) {
    }

    public static BottomPanelControls makeBottomPanel() {
        return makeBottomPanel("OK", "Cancel");
    }

    public static BottomPanelControls makeBottomPanel(String okString, String cancelString) {
        var panel = new JPanel();
        var layout = new BoxLayout(panel, BoxLayout.X_AXIS);

        var cancelButton = new JButton(cancelString);
        var okButton = new JButton(okString);

        panel.add(Box.createHorizontalGlue());
        if (SystemInfo.isWindows) {
            panel.add(okButton);
            panel.add(Padding.makeButtonSeparator());
            panel.add(cancelButton);
        } else {
            panel.add(cancelButton);
            panel.add(Padding.makeButtonSeparator());
            panel.add(okButton);
        }
        panel.setLayout(layout);

        okButton.setMnemonic(KeyEvent.VK_O);
        cancelButton.setMnemonic(KeyEvent.VK_C);

        return new BottomPanelControls(panel, okButton, cancelButton);
    }

    public static void setupCancelBehavior(JDialog frame, JButton button, Runnable action) {
        button.addActionListener(e -> action.run());

        // Run cancel action when cross is clicked
        frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                action.run();
            }
        });

        // Run cancel action on ESCAPE
        frame.getRootPane().registerKeyboardAction(
                e -> action.run(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT
        );
    }
}
