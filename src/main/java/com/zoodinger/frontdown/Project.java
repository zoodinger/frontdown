package com.zoodinger.frontdown;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class Project implements Cloneable {
    @SerializedName("name")
    private String name;

    @SerializedName("local_dir")
    private String localDir;

    @SerializedName("backup_dir")
    private String backupDir;

    @SerializedName("excluded_files")
    private ArrayList<String> excludedFiles;

    @SerializedName("exclude_hidden")
    private boolean excludeHidden;

    public Project() {
        setName("");
        setLocalDir("");
        setBackupDir("");
        setExcludeHidden(false);
        excludedFiles = new ArrayList<>();
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    protected Project clone() {
        Project clone;
        try {
            clone = (Project) super.clone();
        } catch (CloneNotSupportedException e) {
            return new Project();
        }
        clone.excludedFiles = new ArrayList<>(excludedFiles);
        return clone;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getLocalDir(), getBackupDir(), getExcludedFiles());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocalDir() {
        return localDir;
    }

    public void setLocalDir(String localDir) {
        this.localDir = localDir;
    }

    public String getBackupDir() {
        return backupDir;
    }

    public void setBackupDir(String backupDir) {
        this.backupDir = backupDir;
    }

    public List<String> getExcludedFiles() {
        return excludedFiles;
    }

    public void setExcludedFiles(List<String> excludedFiles) {
        this.excludedFiles.clear();
        this.excludedFiles.addAll(excludedFiles);
    }

    public boolean isExcludeHidden() {
        return excludeHidden;
    }

    public void setExcludeHidden(boolean excludeHidden) {
        this.excludeHidden = excludeHidden;
    }
}