# Changelog

## `Version 2.2.2`

### Fixed

- Copyright information displays properly
- Disabled multiple selection of projects

## `Version 2.2.1`

### Changed

- Slight layout improvement in settings window
- The double click action saved in the previous versions is now incompatible
  and will revert to the default option of "Edit Project". It can be changed
  and it will be re-saved again in the new version.
- A lot of internal changes

## `Version 2.2.0`

### Added

- IntelliJ and Darcula flatlaf themes added. They also support embedded menus on windows 10+
- Now detecting and giving the option for additional installed swing themes, which includes
  Metal and any available native themes.

### Changed

- Changing theme settings no longer requires a restart before changes take effect.
- Fallback theme is now flatlaf Light.

## `Version 2.1.1`

### Added

- Button mnemonics for OK, Cancel, Defaults.

### Fixed

- Configuration not getting created on first run because of missing directory. Whoops.

## `Version 2.1.0`

### Added

- New themes: Light (Default), Dark, Metal, and Native.
  Light and dark themes are based on the FlatLaf theme.
- Settings window with options:
    - Change theme
    - Embed menu bar to window title (only for FlatLaf themes and Windows 10+)
    - Double click action

## Version `2.0.0`

### Added

- FlatLaf theme to replace existing look and feel
- Menu bar which contains all available actions
- Shortcut keys and mnemonics (in menu) to trigger actions

### Changed

- Top buttons were removed (functionality is now in menu bar)
- Copy action renamed to Duplicate

### Fixed

- Exclude hidden files and folders checkbox no longer stretches to the right.
- Progress bar now starts from 0% instead of 1%.

## Version `1.4.0`

### Changed

- Visual improvements
- Internal improvements

### Added

- Button mnemonics

## Version `1.3.0`

### Changed

- Main window layout was modified
- Slight modifications to project editor layout

### Added

- About window with version information

## Version `1.2.1`

### Changed

- Some padding between controls was adjusted.

### Fixed

- X button on progress dialog did not close the dialog when pressed after the task
  finished successfully. Now works as intended.

## Version `1.2.0`

### Added

- Copy button to duplicate an existing project.

## Version `1.1.0`

### Added

- Changelog.
- Output backup files now have a `.zip.tmp` extension until they finish, for extra safety.

### Changed

- Interrupted progresses (either by error or being cancelled) are now dealt with
  faster and more robustly:
    - Cancelling a backup progress now more robustly ensures the temporary file is deleted.
    - The user no longer gains control back until the interruption is finalized.
    - Progress no longer waits for files to finish being written.
    - Other internal changes.

# Version `1.0`

- First version.